Rails.application.routes.draw do
  resources :words, only: [:new, :create]
  devise_for :users
  root 'static_page#home'
  get 'dashboard', to: 'static_page#dashboard'
  get 'write', to: 'words#new'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
