class ApplicationMailer < ActionMailer::Base
  default from: 'mail@book_of_secret_words'
  layout 'mailer'
end
