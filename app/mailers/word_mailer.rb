class WordMailer < ApplicationMailer
  def word_mail
    @word = params[:word]
    mail(to: @word.user.email, subject: "Somebody said something about you")
  end
end
