class WordsController < ApplicationController
  before_action :set_word, only: [:show, :edit, :update, :destroy]



  # GET /words/new
  def new
    @user = User.find_by_uid params[:user]
    if @user
      @word = Word.new
      @word.user = @user
    else
      render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found
    end

  end



  # POST /words
  # POST /words.json
  def create
    @word = Word.new(word_params)

    respond_to do |format|
      if @word.save
        WordMailer.with(word: @word).word_mail.deliver_later
        format.html { redirect_to new_user_registration_path,
                                  notice: 'You have successfully said your words. Why not get your own account?' }
      else
        format.html { render :new }

      end
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_word
      @word = Word.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def word_params
      params.require(:word).permit(:user_id, :body)
    end
end
