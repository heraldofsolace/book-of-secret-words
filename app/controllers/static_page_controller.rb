class StaticPageController < ApplicationController
  before_action :authenticate_user!, only: [:dashboard]
  def home
    if user_signed_in?
      redirect_to :dashboard
    end
  end

  def dashboard
    unless current_user.confirmed?
      redirect_to :home, notice: "Your email is not confirmed. Please confirm using the link sent to your email."
    end
  end
end
