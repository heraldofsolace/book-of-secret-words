json.extract! word, :id, :user_id, :body, :created_at, :updated_at
json.url word_url(word, format: :json)
