# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
  $("#urlbtn").on('click', (e) ->
    e.preventDefault()
    console.log($("#url").text())
    $temp = $("<input>")
    $("body").append($temp)
    $temp.val($("#url").text()).select()
    document.execCommand("copy")
    $temp.remove()
    $.notify({
      message:  "Copied"
    })
  )


