FROM ruby:2.6.5
ENV RAILS_ENV production
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs && mkdir /app
WORKDIR /app
COPY Gemfile /app/Gemfile
COPY Gemfile.lock /app/Gemfile.lock
RUN bundle install --deployment --without development test
COPY . /app
RUN bundle exec rake assets:precompile
EXPOSE 3000
#ENV POSTGRES_PASSWORD ${POSTGRES_PASSWORD}
ENTRYPOINT ["./entrypoint.sh"]