FactoryBot.define do
  factory :word do
    body { Faker::Quotes::Shakespeare.hamlet_quote }
    user
  end
end