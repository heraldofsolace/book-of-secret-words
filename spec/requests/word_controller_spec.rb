require 'rails_helper'

RSpec.describe "Word management", type: :request do
  it "creates a word for an existing user" do
    word = build(:word)
    get "/write?user=#{word.user_id}"
    expect(response).to render_template :new
    post "/words", params: { word: { user_id: word.user_id, body: word.body } }
    expect(Word.count).to eq 1
    expect(word.user.words.count).to eq 1
  end

  it "gives error if user doesn't exist" do
    get "/write?user=100"
    expect(response).to have_http_status 404
  end
end